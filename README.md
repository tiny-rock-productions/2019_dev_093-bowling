# 2019_DEV_093/Bowling

This repository contains an XCode project.
In order to run this project you will need to have XCode installed on your machine.

After cloning or downloading the project to your hard drive, open up the project by double clicking the bowlingGame.xcodeproj file.
After the project has opened in XCode you can run the tests by using "command + U".
If everything goes well the project will compile and execute the tests.
Hopefully the tests will all run succesfully and validate the business logic that has been written in BowlingGameViewModel.swift

This project was written in Swift 4.2 using a Test Driven Development method.
//
//  BowlingGameViewModel.swift
//  bowlingGame
//
//  Created by Dwayne Slootmans on 11/02/2019.
//  Copyright © 2019 Devoteam. All rights reserved.
//

import Foundation

class BowlingGameViewModel {
    static let sharedInstance = BowlingGameViewModel()
    
    private var game: Game?
    
    func startNewGame(frames: [Frame]) {
        self.game = Game(frames: frames)
    }
    
    func getScore() -> Int {
        guard let game = game else { return 0 }
        var score = 0
        
        for i in 0...game.frames.count - 1 {
            if i - 1 >= 0 && i <= Constants.MAX_NUMBER_OF_GAMES { // We don't get any extra points after the 11th frame
                // If 1 game ago we had a strike or a spare we're getting a bonus on this frame
                if game.frames[i - 1].scoreType == .strike || game.frames[i - 1].scoreType == .spare {
                    score += game.frames[i].firstScore + game.frames[i].secondScore
                }
            }
            if i - 2 >= 0 {
                // If 2 frames ago we had a strike we're still getting a bonus from this frame
                if game.frames[i - 2].scoreType == .strike {
                    score += game.frames[i].firstScore + game.frames[i].secondScore
                }
            }
            // The score of the current frame is always added to the total score
            // Except when you are on the 11th or 12th roll, then it only gets added as extra score to the 10th
            if i < Constants.MAX_NUMBER_OF_GAMES {
                score += game.frames[i].firstScore + game.frames[i].secondScore
            }
        }
        
        return score
    }
    
}

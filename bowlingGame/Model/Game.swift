//
//  Game.swift
//  bowlingGame
//
//  Created by Dwayne Slootmans on 11/02/2019.
//  Copyright © 2019 Devoteam. All rights reserved.
//

import Foundation

struct Game {
    var frames:[Frame]
    
    init(frames: [Frame]) {
        self.frames = frames
    }
}

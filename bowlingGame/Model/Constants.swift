//
//  Constants.swift
//  bowlingGame
//
//  Created by Dwayne Slootmans on 12/02/2019.
//  Copyright © 2019 Devoteam. All rights reserved.
//

import Foundation

struct Constants {
    static let MAX_NUMBER_OF_GAMES = 10
}

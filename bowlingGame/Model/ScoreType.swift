//
//  ScoreTypes.swift
//  bowlingGame
//
//  Created by Dwayne Slootmans on 11/02/2019.
//  Copyright © 2019 Devoteam. All rights reserved.
//

import Foundation

enum ScoreType {
    case strike
    case spare
    case open
}

//
//  Frame.swift
//  bowlingGame
//
//  Created by Dwayne Slootmans on 11/02/2019.
//  Copyright © 2019 Devoteam. All rights reserved.
//

import Foundation

struct Frame {
    var scoreType: ScoreType
    var firstScore: Int
    var secondScore: Int
    
    init(firstScore: Int, secondScore: Int) {
        self.firstScore = firstScore
        self.secondScore = secondScore
        
        if firstScore == 10 { // If the first score is 10 then we are dealing with a strike
            self.scoreType = .strike
        } else if firstScore + secondScore == 10 { // If the first and second score combined is 10 then it's a spare
            self.scoreType = .spare
        } else { // Anything else will be open
            self.scoreType = .open
        }
    }
}

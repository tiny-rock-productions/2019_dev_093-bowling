//
//  bowlingGameTests.swift
//  bowlingGameTests
//
//  Created by Dwayne Slootmans on 11/02/2019.
//  Copyright © 2019 Devoteam. All rights reserved.
//

import XCTest
@testable import bowlingGame

class bowlingGameTests: XCTestCase {

    private var viewModel: BowlingGameViewModel?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        viewModel = BowlingGameViewModel.sharedInstance
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAllZero() {
        var frames = [Frame]()
        for _ in 1...10 {
            frames.append(Frame(firstScore: 0, secondScore: 0))
        }
        XCTAssertEqual(frames[0].scoreType, .open)
        viewModel?.startNewGame(frames: frames)
        XCTAssertEqual(viewModel?.getScore(), 0)
    }
    
    func testAllThrees() {
        var frames = [Frame]()
        for _ in 1...10 {
            frames.append(Frame(firstScore: 3, secondScore: 3))
        }
        XCTAssertEqual(frames[0].scoreType, .open)
        viewModel?.startNewGame(frames: frames)
        XCTAssertEqual(viewModel?.getScore(), 60)
    }
    
    func testAllStrikes() {
        var frames = [Frame]()
        for _ in 1...12 {
            frames.append(Frame(firstScore: 10, secondScore: 0))
        }
        XCTAssertEqual(frames[0].scoreType, .strike)
        viewModel?.startNewGame(frames: frames)
        XCTAssertEqual(viewModel?.getScore(), 300)
    }
    
    func testAllSpares() {
        var frames = [Frame]()
        for _ in 1...11 {
            frames.append(Frame(firstScore: 5, secondScore: 5))
        }
        XCTAssertEqual(frames[0].scoreType, .spare)
        viewModel?.startNewGame(frames: frames)
        XCTAssertEqual(viewModel?.getScore(), (200))
    }
    
    func testRandomScore() {
        var frames = [Frame]()
        frames.append(Frame(firstScore: 10, secondScore: 0)) // 25
        frames.append(Frame(firstScore: 2, secondScore: 3)) // 5
        frames.append(Frame(firstScore: 4, secondScore: 6)) // 15
        frames.append(Frame(firstScore: 5, secondScore: 0)) // 5
        frames.append(Frame(firstScore: 0, secondScore: 4)) // 4
        frames.append(Frame(firstScore: 10, secondScore: 0)) // 22
        frames.append(Frame(firstScore: 10, secondScore: 0)) // 19
        frames.append(Frame(firstScore: 2, secondScore: 0)) // 2
        frames.append(Frame(firstScore: 4, secondScore: 3)) // 7
        frames.append(Frame(firstScore: 0, secondScore: 1)) // 1
        // This all adds up to                                 105
        viewModel?.startNewGame(frames: frames)
        XCTAssertEqual(viewModel?.getScore(), 105)
    }

}
